import PyPDF2
import nltk
from nltk.corpus import stopwords
import re
from nltk.tokenize import word_tokenize
import string
from nltk.stem.wordnet import WordNetLemmatizer
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
import gensim


## GUI Window
a = Tk()
a.title("Book Indexer")
a.resizable(False,False)
a.geometry('700x400')
a.iconbitmap('C:\\Users\\Oluwaseun\\Pictures\\Project Images\\documents-indexing-and-archiving-services.png')
C = Canvas(a, bg="blue", height=400, width=700)
filename = PhotoImage(file = "C:\\Users\\Oluwaseun\\Pictures\\Project Images\\Indexingp.png")
C.create_image(0, 0, image=filename, anchor='nw')
C.create_text(350,50, text="BOOK INDEXING SYSTEM", font=("Purisa", 20),fill="white")
C.pack()
## Opening file dialog window
global book_words

def importbook():
    global file1
    global filer
    file1 = filedialog.askopenfile(mode="rb",title='Select a pdf file')#a variable that stores the file type filedialog is a class in tkinter and askopenfilename is a function that opens files in any filetype
    filer = file1.name
    text1 = Text(a)
    text1.insert(END,filer)
    text1.config(width = 35, height =2,state=DISABLED)
    text1_window = C.create_window(370, 118, anchor=NW, window=text1)

    if not filer.endswith('.pdf'):
        messagebox.showinfo("Visualizer error","File must be a .pdf")
    else:
        return file1
        return filer
        print(file1)

def index():
    newwin = Toplevel(a,bg ="#3399ff")
    newwin.title("Index Terms")
    scrollbar = Scrollbar(newwin)
    scrollbar.pack(side = RIGHT, fill =Y)
    ##    Scrollbar.pack(side = BOTTOM, fill =X)
        
                
    mylist = Listbox(newwin, yscrollcommand = scrollbar.set,width=100, height=20)
    label = Label(newwin, text="IndexTerms")

    f = open(filer,"rb")
    pdfreader = PyPDF2.PdfFileReader(f)
    NumPages = pdfreader.getNumPages()
    stopw = stopwords.words('english')
    other = ['']
    punc = string.punctuation
    ##print(stopwords)
    ##print(punc)
    to_exclude = [*stopw, *other, *punc]
        
    global book_words
    book_words = {}
    for page_no in range(0, NumPages):
    # get words on page
        page = pdfreader.getPage(page_no)
        page_text= page.extractText()
        page_text = nltk.word_tokenize(page_text)
        page_text = str(page_text).lower()
        page_text = page_text.lstrip()
        ##    page_text = re.sub('[0-9 \'"“”•©><#‘’.,]+', '', page_text) # remove punctuation and other special chars
        #page_text = re.sub(' ( [ 0-9 \ '#−"•!+“”&&$!=‘~©°}~)×/*’¬….–,−><]|--)', '', page_text) # remove punctuation and other special chars
        page_text = re.sub('\n', ' ', page_text) # change line breaks to a single space
        page_text = re.sub('\s\s+' , ' ', page_text) # remove multiple spaces
        page_text = re.sub('[^A-Za-z]+', ' ', page_text)#regex to match a string of characters that are not a letters or numbers
        ##    page_text = re.sub('[z]', ' ', page_text)
        bigram = gensim.models.Phrases(page_text,min_count=5, threshold=100)# higher threshold fewer phrases
        trigram = gensim.models.Phrases(bigram[page_text], threshold=100)
        # Faster way to get a sentence clubbed as a trigram/bigram
        bigram_mod = gensim.models.phrases.Phraser(bigram)
        trigram_mod = gensim.models.phrases.Phraser(trigram)
        # See trigram example
        page_txt = trigram_mod[bigram_mod[page_text[0]]]
        page_txxt = str(page_text)
        print(str(page_txt))
        page_words = page_txxt.split(' ')
        lem = WordNetLemmatizer()
        page_text = lem.lemmatize(str(page_words),"v")


                # filter out stop words, punctuations, symbols etc
        filtered_words = []
        for word in page_words:
            if word not in to_exclude:
                filtered_words.append(word)

                # add filtered words, their frequency and page number
        for word in filtered_words:
            if word in book_words:
                book_words[word]['frequency'] += 1
                if page_no not in book_words[word]['appears_in_pages']:
                    book_words[word]['appears_in_pages'].append(page_no)
            else:
                book_words[word] = {
                    'frequency': 1,
                    'appears_in_pages': [page_no],
                }
    for word, meta in sorted(book_words.items()):
        if meta['frequency'] >= 3:
            IndexTerms = word, meta
            print(IndexTerms)
                    
            mylist.insert(END, IndexTerms)
            mylist.pack()
            scrollbar.config( command = mylist.yview )
        ##            scrollbar.config( command = mylist.xview )           
    messagebox.showinfo("Success","Successful index")          
def exportbook():
    file2 = filedialog.asksaveasfile(mode='w', title='Save file', filetypes=(("Microsoft word file", '*.doc'),("Text file", '*.txt')))
    if file2 is None:
        return
    else:
        output_str = 'Index Term,PageNumbers\n'
        print(output_str)
        file2.write(output_str)
        for word, meta in sorted(book_words.items()):
            if meta['frequency'] >= 5:
                output_strn = word + ',' + str(meta['appears_in_pages']) + '\n'
                file2.write(output_strn)
                print(output_strn)

##if importbook()
button1 = Button(a,text="IMPORT BOOK",command = importbook)
button1.configure(width = 20,height = 3,bg="#3399ff",fg="white")
button1_window = C.create_window(20, 120, anchor=NW, window=button1)
button2 = Button(C,text="INDEX BOOK", command = index)
button2.configure(width = 20,height = 3, bg="#3399ff",fg ="white" )
button2_window = C.create_window(20, 180, anchor=NW, window=button2)
button3 = Button(C,text="EXPORT BOOK" ,command = exportbook)
button3.configure(width = 20,height = 3 ,bg="#3399ff",fg ="white")
button3_window = C.create_window(20, 240, anchor=NW, window=button3)





##print(root.fileName)

# nltk.download('stopwords')



##     stop at page 10
##            if page_no >= 10:
##                break

a.mainloop()
